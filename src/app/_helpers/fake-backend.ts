﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError, GroupedObservable } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
// localStorage.clear();
let users = JSON.parse(localStorage.getItem('users')) || [];
// console.log("USERS===",users);
let employees = JSON.parse(localStorage.getItem('Employees')) || [];
@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users/register') && method === 'POST':
                    return register();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.endsWith('/employees') && method === 'GET':
                return getAllEmpoyees();
                case url.endsWith('/users/employee') && method === 'POST':
                    return createEmployee();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                case url.endsWith('/Updateusers') && method === 'PUT':
                    return updateUser();   
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }    
        }

        // route functions

        function authenticate() {
            const { email, password } = body;
            const user = users.find(x => x.email === email && x.password === password);
            if (!user) return error('email or password is incorrect');
            return ok({
                id: user.id,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                // userrole:user.userrole,
                address:user.address,
                company:user.company,
                dob:user.dob,
                token: 'fake-jwt-token'
            })
        }

        function register() {
            // console.log("USERS===",users);
            const user = body;
            if (users.find(x => x.email === user.email)) {
                return error('email "' + user.email + '" is already taken')
            }
            user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(user);
            localStorage.setItem('users', JSON.stringify(users));

            return ok();
        }

        function getUsers() {
            if (!isLoggedIn()) return unauthorized();
            return ok(users);
        }
        function getAllEmpoyees() {
            if (!isLoggedIn()) return unauthorized();
            return ok(employees);
        }
        function createEmployee() {
            const emp = body;
            emp.id = employees.length ? Math.max(...employees.map(x => x.id)) + 1 : 1;
            employees.push(emp);
            localStorage.setItem('employees', JSON.stringify(employees));
            return ok(employees);
        }
        function deleteUser() {
            if (!isLoggedIn()) return unauthorized();
            employees = employees.filter(x => x.id !== idFromUrl());
            localStorage.setItem('Employees', JSON.stringify(employees));
            return ok();
        }
        function updateUser() {
            let emp = body;
            if (!isLoggedIn()) return unauthorized();
            if(employees){
            for(let i=0;i<=employees.length;i++){
                if(employees[i].id == emp.id){
                    employees[i].firstname = emp.firstname;
                    employees[i].lastname = emp.lastname;
                    employees[i].email = emp.email;
                    employees[i].address = emp.address;
                    employees[i].dob = emp.dob;
                    employees[i].mob = emp.mob;
                    employees[i].city = emp.city;
                    localStorage.setItem('Employees', JSON.stringify(employees));
                }
            } 
            return ok(employees );
        }
        }
        // helper functions

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message) {
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
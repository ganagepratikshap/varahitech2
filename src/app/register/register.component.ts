﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators,FormArray,FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService, AuthenticationService } from '@/_services';

@Component({ templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            // console.log("this.authenticationService.currentUserValue",this.authenticationService.currentUserValue);
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required,Validators.email]],
            company: ['', Validators.required],
            dob: ['', Validators.required],
            address: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            userrole: this.formBuilder.array([])
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onChange(role: string, isChecked: boolean) {
        const emailFormArray = <FormArray>this.registerForm.controls.userrole;
    
        if (isChecked) {
          emailFormArray.push(new FormControl(role));
        } else {
          let index = emailFormArray.controls.findIndex(x => x.value == role)
          emailFormArray.removeAt(index);
        }
       
      }
    onSubmit() {
        this.submitted = true;
        // reset alerts on submit
        this.alertService.clear();
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.loading = true;
        // console.log("register user value in onsubmit fun",this.registerForm.value)
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
